package BLL;

import DLL.SSHConnectionManager;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author promise7091
 */
public class AppRunner {

    public static void main(String[] args) throws FileNotFoundException {

        List<String> commands = new ArrayList<>();
        commands.add("ls");

        //Enter remote host IP
        SSHConnectionManager.setHostname("");
        //Enter username
        SSHConnectionManager.setUsername("");
        //Enter password
        SSHConnectionManager.setPassword("");

        System.out.println(SSHConnectionManager.executeCommands(commands));

        SSHConnectionManager.close();
    }
}
